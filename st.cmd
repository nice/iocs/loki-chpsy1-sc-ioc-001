#!/usr/bin/env iocsh.bash
require(modbus)
require(s7plc)
require(calc)
epicsEnvSet(loki-chpsy1-sc-ioc-001_VERSION,"plcfactory")
iocshLoad("$(essioc_DIR)/essioc.iocsh")
iocshLoad("./loki-chpsy1-sc-ioc-001.iocsh","IPADDR=172.30.235.35,RECVTIMEOUT=3000")

#Load alarms database
epicsEnvSet(P,"LOKI-ChpSy1:")
epicsEnvSet(R1,"Chop-BWC-101:")
epicsEnvSet(R2,"Chop-BWC-102:")
epicsEnvSet(R3,"Chop-RIC-101:")
epicsEnvSet(R4,"Chop-RIC-102:")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R1)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R2)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R3)")
dbLoadRecords("./SKFAlrm.db", "P=$(P), R=$(R4)")

iocInit()
#EOF
